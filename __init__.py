import os

ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# source folder
ASSETS=os.path.join(ROOT,'com.ujoy.dbcv')
# destination folder
DST = os.path.join(ROOT, 'extracted')